<?php
		session_start();

		$validForm = false;

    $inProductName = "";
    $inProductDescription = "";
    $inProductPrice = "";
    $inProductType = "";
    $inProductFullImage = "";
    $inProductThumbImage = "";
    $inProductNumber = "";

		$productIdToUpdate = "";
		$sqlUpdate = "";

		$updateHeading = "";
    $imageMsg = "";
		$resultMsg = "";
		$productNameErrMsg = "";
		$productDescriptionErrMsg = "";
		$productTypeErrMsg = "";
		$productPriceErrMsg = "";
		$productFullImageErrMsg = "";
    $productThumbErrMsg = "";
    $productNumberErrMsg = "";

		//Bring in Validation and Image Upload classes
    include 'classes/validations.php';
    include 'classes/imageUpload.php';

	if ($_SESSION['validUser'] == "yes") {
    if ($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 2) {

      include "connectPDO.php";  //Connects to the database and inserts the data

      if ( isset($_POST['submitBtn']) ) {	 /*If the form has been posted or submitted*/

					//Create validation and image upload objects
          $validations = new Validations();
          $imageUpload = new ImageUpload();
        /*Get the form data from the $_POST variable and place in PHP variables*/

          $inProductName = $_POST['product_title'];
          $inProductDescription = $_POST['product_description'];
          $inProductPrice = $_POST['product_price'];
          $inProductType = $_POST['product_type'];

//If a file was submitted then get file information.  Then pass information to image upload object and store the result.
          if($_FILES['uploadImage']['error']==0) {
            $file_name = $_FILES['uploadImage']['name'];
            $file_size = $_FILES['uploadImage']['size'];
            $file_tmp = $_FILES['uploadImage']['tmp_name'];
            $file_type= $_FILES['uploadImage']['type'];

            $imageUpload->set_filename($file_name);
            $imageUpload->set_filesize($file_size);
            $imageUpload->set_filetmp($file_tmp);
            $imageUpload->set_filetype($file_type);
            $imageUpload->set_productType($inProductType);

            $productFullImageErrMsg = $imageUpload->uploadFullImage();

            $inProductFullImage = $imageUpload->get_fullPath();

						//$_SESSION['fullImagePath'] = $inProductFullImage;

          }

//If a file was submitted then get file information.  Then pass information to image upload object and store the result.
          if($_FILES['uploadThumb']['error']==0){
            $thumb_name = $_FILES['uploadThumb']['name'];
            $thumb_size = $_FILES['uploadThumb']['size'];
            $thumb_tmp = $_FILES['uploadThumb']['tmp_name'];
            $thumb_type= $_FILES['uploadThumb']['type'];

            $imageUpload->set_thumbname($thumb_name);
            $imageUpload->set_thumbsize($thumb_size);
            $imageUpload->set_thumbtmp($thumb_tmp);
            $imageUpload->set_thumbtype($thumb_type);
            $imageUpload->set_productType($inProductType);

            $productThumbErrMsg = $imageUpload->uploadThumbImage();

            $inProductThumbImage = $imageUpload->get_thumbPath();

						//$_SESSION['thumbImagePath'] = $inProductThumbImage;
          }

          if (isset($_GET['prod_no'])) {
        /*Get the event ID from the $_GET variable that was passed from the action attribute in the form*/
              $inProductNumber = $_GET['prod_no'];
              $updateHeading = "<h1>Updating Product:  $inProductNumber</h1>";
              //$imageMsg = "<h5 style = 'color: #c17524;'>Notice! When updating a product, make sure to reselect image files even if you do not plan on using different images. Due to security concerns, browsers do not allow these fields to be selected by anyone but the user.</h4>";
          } else {
              $updateHeading = "<h1>Add Product:</h1>";
          }
        /*Validate the form data or at least sanitize the data*/
          $validForm = true;

          $validations->set_name($inProductName);
  				$validations->set_price($inProductPrice);
  				$validations->set_userMsg($inProductDescription);
          $validations->set_productType($inProductType);
  				$validations->set_validForm($validForm);

  				$productNameErrMsg = $validations->validateName();
  				$productPriceErrMsg = $validations->validatePrice();
  				$productDescriptionErrMsg = $validations->validateProductDescription();
          $productTypeErrMsg = $validations->validateProductType();

  				$validForm = $validations->get_validForm();
  				$inProductName = $validations->get_name();
          $inProductPrice = $validations->get_price();
          $inProductDescription = $validations->get_userMsg();
          $inProductType = $validations->get_productType();

      } //End if form submitted

      elseif (isset($_GET['prod_no'])) {
          //If there is a product number value in the Get variable
          //Do update

          $inProductNumber = $_GET['prod_no'];

          $updateHeading = "<h1>Updating Product:  $inProductNumber</h1>";
          //$imageMsg = "<h5 style = 'color: #c17524;'>Notice! When updating a product, make sure to reselect image files even if you do not plan on using different images. Due to security concerns, browsers do not allow these fields to be selected by anyone but the user.</h4>";

          /*Create a SELECT query to retrieve the requested record from the database table*/

          $sqlSelect = "SELECT product_title, product_price, product_description, product_image_path, product_thumb_path, product_type, product_number FROM ecomm_products WHERE product_number = $inProductNumber";

          //Run SELECT query

          try {
              $stmt = $conn->prepare($sqlSelect);

              $stmt->execute();
          } catch (PDOException $e) {

              $resultMsg = "<p>There was an error with your request: " . $e->getMessage() . "</p>";
          }

          if ($stmt->execute()){  /*If select query was successful and there is content*/
          //Place content from database into form fields
          //Display the form to the user with content in the fields
              while($row = $stmt->fetch()) {

                  $inProductName = $row['product_title'];
                  $inProductDescription = $row['product_description'];
                  $inProductType = $row['product_type'];
                  $inProductPrice = $row['product_price'];
                  $inProductFullImage = $row['product_image_path'];
                  $inProductThumbImage = $row['product_thumb_path'];

									$_SESSION['fullImagePath'] = $inProductFullImage;
									$_SESSION['thumbImagePath'] = $inProductThumbImage;
              }
          }
      } else {
          $updateHeading = "<h1>Add Product:</h1>";
      }

    		if ($validForm) {  /*If data validation passes...*/
          if (isset($_GET['prod_no'])) {  // if product number is present in Get variable then update the info in database with new values
    				$sqlUpdate = "UPDATE ecomm_products SET ";
    				$sqlUpdate .= "product_title = '$inProductName', ";
    				$sqlUpdate .= "product_description = '$inProductDescription', ";
    				$sqlUpdate .= "product_type = '$inProductType', ";
						if ($inProductFullImage != "") {
    					$sqlUpdate .= "product_image_path = '$inProductFullImage', ";
						}
						if ($inProductThumbImage != "") {
            	$sqlUpdate .= "product_thumb_path = '$inProductThumbImage', ";
						}
						$sqlUpdate .= "product_price = '$inProductPrice' ";
    				$sqlUpdate .= "WHERE (product_number = '$inProductNumber')";

    				$updateStmt = $conn->prepare($sqlUpdate);

    				//Run the update query

    				$updateStmt->execute();

    				if ($updateStmt->execute()){  /*If update query was successful*/

    						//Confirm to the user that their data has been successfully processed
    						//Provide a link to the home page and the select page
    						$resultMsg =  "<h1>Success! The product has been updated.</h1>";
                $resultMsg .= "<p><a class='btn btn-info btn-lg' href = 'add-updateProduct.php'>Add A Product</a>";
                $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'manageProducts.php'>Back to Products List</a>";
                $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'logout.php'>Logout</a></p>";
    				}

    				else { /*If there was an error with the query and did not execute*/
    						$resultMsg = "<h1>Update error: " . $e->getMessage() . "</h1>";
    						//Display message “Oops Please try again”
    						//Display a link to the home page and select page

    				}
          } elseif (!isset($_GET['prod_no'])) { // If user pressed "Add Product" button
            try {
								// add product to database
              $sqlAdd = "INSERT INTO ecomm_products (product_title, product_price, product_description, product_image_path, product_thumb_path, product_type) VALUES (:product_title, :product_price, :product_description, :product_image_path, :product_thumb_path, :product_type)";

              $sqlPrepareAdd = $conn->prepare($sqlAdd);

              $sqlPrepareAdd->bindParam(':product_title', $inProductName);
              $sqlPrepareAdd->bindParam(':product_price', $inProductPrice);
              $sqlPrepareAdd->bindParam(':product_description', $inProductDescription);
              $sqlPrepareAdd->bindParam(':product_image_path', $inProductFullImage);
              $sqlPrepareAdd->bindParam(':product_thumb_path', $inProductThumbImage);
              $sqlPrepareAdd->bindParam(':product_type', $inProductType);

            } catch (PDOException $e) {
                echo "There was a problem entering the information.  Please try again: " . $e->getMessage();
            }

            if ($sqlPrepareAdd->execute()){

                  $resultMsg = "<h4>The new product has been added.</h4>";
                  $resultMsg .= "<p><a class='btn btn-info btn-lg' href = 'add-updateProduct.php'>Add Another Product</a>";
                  $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'manageProducts.php'>Back to Products List</a>";
                  $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'logout.php'>Logout</a></p>";
            }
          }
    		}

    } else {

  			header('Location: login.php');

  	}
	} else {

			header('Location: login.php');

	}


?>
<!DOCTYPE html>
	<html lang="en">
		<head>
		  <title>Products - Outdated Phones</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">

		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
		  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
			<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

		  <style>
					.updateImg{
							width: 90%;
							height: auto;
							margin: 0 auto;
					}
		  </style>
		</head>


		<body>

				<div class="jumbotron">
				  <div class="container text-center">
				    <h1>Outdated Phones</h1>
				    <p>We sell everything but smartphones!</p>
				  </div>
				</div>

				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
				    </div>
				    <div class="collapse navbar-collapse" id="myNavbar">
				      <ul class="nav navbar-nav">
				        <li><a href="storeHome.php">Home</a></li>
				        <li><a href="storeProducts.php">Products</a></li>
				        <li><a href="storeContact.php">Contact</a></li>
				      </ul>
				      <ul class="nav navbar-nav navbar-right">
										<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

						<li>
						<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
								<!-- Identify your business so that you can collect the payments. -->
								<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

								<!-- Specify a PayPal shopping cart View Cart button. -->
								<input type="hidden" name="cmd" value="_cart">
								<input type="hidden" name="display" value="1">

								<!-- Display the View Cart button. -->
								<input type="image" name="submit" 
									src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
								alt="Add to Cart" style="margin-top:10px;">
								<img alt="" width="1" height="1"
									src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
							</form>				
					</li>
						</ul>
					</div>
					</div>
				</nav>

        <div class = "container">
<?php
		//If the user submitted the form the changes have been made
		//if ( isset($_POST['submitBtn']) ) {
      if ($validForm) {
				echo $resultMsg;	//contains a Success or Failure output content
      }
		//}//end if submitted

		else
		{	//The page needs to display the form and associated data to the user for changes
			$inProductFullImage = $_SESSION['fullImagePath'];
			$inProductThumbImage = $_SESSION['thumbImagePath'];
				echo $updateHeading;
?>
								 <form id="form1" name="form1" method="post" action="add-updateProduct.php<?php if (isset($_GET['prod_no'])){echo '?prod_no='.$inProductNumber;}?>" enctype="multipart/form-data">

                   <div class = "row">
                     <div class = "col-sm-5">

										<p>Product Name: <span class = "error"><?php echo "$productNameErrMsg"; ?></span><br>
												<input type="text" name="product_title" id="productName" value="<?php echo $inProductName;?>"/>
										</p>

										<p>Product Description:  <span class = "error"><?php echo "$productDescriptionErrMsg"; ?></span><br>
												<textarea name="product_description" id = "productDescription" rows="5" cols="40"><?php echo $inProductDescription;?></textarea>
										</p>

                    <p>Product Type:    <span class = "error"><?php echo "$productTypeErrMsg"; ?></span><br>
												<label>
													  <select name="product_type" id="productType">
    														<option value="Brick Phone" <?php if($inProductType == "Brick Phone"){echo "selected = 'selected'";}?>>Brick Phone</option>
    														<option value="Cordless Phone" <?php if($inProductType == "Cordless Phone"){echo "selected = 'selected'";}?>>Cordless Phone</option>
    														<option value="Flip Phone" <?php if($inProductType == "Flip Phone"){echo "selected = 'selected'";}?>>Flip Phone</option>
                                <option value="Slider Phone" <?php if($inProductType == "Slider Phone"){echo "selected = 'selected'";}?>>Slider Phone</option>
                                <option value="Wired Phone" <?php if($inProductType == "Wired Phone"){echo "selected = 'selected'";}?>>Wired Phone</option>
													  </select>
												</label>
											</p>

										<p>Price:  <span class = "error"><?php echo "$productPriceErrMsg"; ?></span><br>
												<input type = "text" name = "product_price" id = "productPrice" value="<?php echo $inProductPrice;?>"/>
										</p>
                    <?php //echo $imageMsg;?>
                    <p>
                        Choose Fullsize Image File: <span class = "error"><?php echo "$productFullImageErrMsg"; ?></span><br>
                        <input type="file" name="uploadImage" id = "imgFull" accept="image/*" />
                    </p>

                    <p>
                        Choose Thumbnail Image File: <span class = "error"><?php echo "$productThumbErrMsg"; ?></span><br>
                        Select a file: <input type="file" name="uploadThumb" id = "imgThumb" accept="image/*" />
                    </p>

										<p>
												<input type="submit" class="btn btn-info btn-lg" name="submitBtn" id="submitBtn" value="Submit" />
												<input type="reset" class="btn btn-info btn-lg" name="resetBtn" id="resetBtn" value="Clear Form" />
										</p>
                  </div>

<?php
      if (isset($_GET['prod_no'])) {  //If a product number is present in the Get variable then show the product image and thumbnail
?>

                  <div class = "col-sm-7" style = "border: 3px solid black;">
                      <p>Fullsize Image:</p><p><img class = "updateImg" src = <?php echo $inProductFullImage;?> />
                      <p>Thumbnail Image:</p><p><img class = "updateImg" src = <?php echo $inProductThumbImage;?> /></p>

                  </div>
<?php
      }
?>
                  </div>
								</form>
                <hr />
								<p>
										<a class="btn btn-info btn-lg" href = "manageProducts.php">Go Back to Table</a>

										<a class="btn btn-info btn-lg" style = "margin-left: 15px;" href='logout.php'>Logout of Admin System</a>
								</p>

						</div>

<?php
		}//end else submitted

		$conn = null;

?>
				</body>

		</html>

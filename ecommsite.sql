-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2017 at 06:44 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `ecomm_contact_messages`
--

CREATE TABLE `ecomm_contact_messages` (
  `message_id` int(11) NOT NULL,
  `message_sender_name` text NOT NULL,
  `message_sender_email` text NOT NULL,
  `message_subject` text NOT NULL,
  `message_body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecomm_contact_messages`
--

INSERT INTO `ecomm_contact_messages` (`message_id`, `message_sender_name`, `message_sender_email`, `message_subject`, `message_body`) VALUES
(1, 'Test Test', 'test@test.com', 'account', 'This is a test message.'),
(6, 'asfdasdf', 'asfdas@fdsdafs.com', 'account', 'asdfasfasfasffasasdfasdf');

-- --------------------------------------------------------

--
-- Table structure for table `ecomm_newsletter`
--

CREATE TABLE `ecomm_newsletter` (
  `newsletter_id` int(11) NOT NULL,
  `newsletter_email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecomm_newsletter`
--

INSERT INTO `ecomm_newsletter` (`newsletter_id`, `newsletter_email`) VALUES
(1, 'aasdfasdf@a.com'),
(2, 'lasdlfk@asdflk.net'),
(5, 'zzzzzzzz@zzz.com'),
(7, 'asdfasdflk@asdflkasdf.com'),
(8, 'test@test.com');

-- --------------------------------------------------------

--
-- Table structure for table `ecomm_products`
--

CREATE TABLE `ecomm_products` (
  `product_number` int(15) NOT NULL,
  `product_title` text NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `product_description` text NOT NULL,
  `product_image_path` varchar(1500) NOT NULL,
  `product_thumb_path` varchar(1500) NOT NULL,
  `product_type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecomm_products`
--

INSERT INTO `ecomm_products` (`product_number`, `product_title`, `product_price`, `product_description`, `product_image_path`, `product_thumb_path`, `product_type`) VALUES
(1, 'Flip Phone 1', '65.99', 'This is a product.', 'img/products/flip/flip1.jpg', 'img/products/flip/thumb/flip1_sm.jpg', 'Flip Phone'),
(2, 'Flip Phone 2', '85.00', 'This is a product', '"img/products/flip/flip2.jpg"', '"img/products/flip/thumb/flip2_sm.jpg"', 'Flip Phone'),
(3, 'Flip Phone 3', '42.50', 'This is a product', '"img/products/flip/flip3.jpg"', '"img/products/flip/thumb/flip3_sm.jpg"', 'Flip Phone'),
(4, 'Flip Phone 4', '90.00', 'This is a product', '"img/products/flip/flip4.jpg"', '"img/products/flip/thumb/flip4_sm.jpg"', 'Flip Phone'),
(5, 'Brick Phone 1', '50.00', 'This is a product', '"img/products/brick/brick1.jpg"', '"img/products/brick/thumb/brick1_sm.jpg"', 'Brick Phone'),
(6, 'Brick Phone 2', '43.00', 'This is a product', '"img/products/brick/brick2.png"', '"img/products/brick/thumb/brick2_sm.png"', 'Brick Phone'),
(7, 'Brick Phone 3', '66.00', 'This is a product', '"img/products/brick/brick3.jpg"', '"img/products/brick/thumb/brick3_sm.jpg"', 'Brick Phone'),
(8, 'Brick Phone 4', '72.00', 'This is a product', '"img/products/brick/brick4.jpg"', '"img/products/brick/thumb/brick4_sm.jpg"', 'Brick Phone'),
(9, 'Brick Phone 5', '88.00', 'This is a product', '"img/products/brick/brick5.jpg"', '"img/products/brick/thumb/brick5_sm.jpg"', 'Brick Phone'),
(10, 'Cordless Phone 1', '34.00', 'This is a product.', '"img/products/cordless/cordless1.png"', '"img/products/cordless/thumb/cordless1_sm.png"', 'Cordless Phone'),
(11, 'Cordless Phone 2', '40.00', 'This is a product.', '"img/products/cordless/cordless2.jpg"', '"img/products/cordless/thumb/cordless2_sm.jpg"', 'Cordless Phone'),
(12, 'Cordless Phone 3', '49.99', 'This is a product.', '"img/products/cordless/cordless3.jpg"', '"img/products/cordless/thumb/cordless3_sm.jpg"', 'Cordless Phone'),
(13, 'Slider Phone 1', '83.00', 'This is a product.', '"img/products/slider/slider1.jpg"', '"img/products/slider/thumb/slider1_sm.jpg"', 'Slider Phone'),
(14, 'Slider Phone 2', '75.25', 'This is a product.', '"img/products/slider/slider2.png"', '"img/products/slider/thumb/slider2_sm.png"', 'Slider Phone'),
(15, 'Wired Phone 1', '100.00', 'This is a product.', '"img/products/wired/wired1.jpg"', '"img/products/wired/thumb/wired1_sm.jpg"', 'Wired Phone'),
(16, 'Wired Phone 2', '150.00', 'This is a product.', '"img/products/wired/wired2.jpg"', '"img/products/wired/thumb/wired2_sm.jpg"', 'Wired Phone'),
(17, 'Wired Phone 3', '450.00', 'This is a product.', '"img/products/wired/wired3.jpg"', '"img/products/wired/thumb/wired3_sm.jpg"', 'Wired Phone'),
(18, 'Wired Phone 4', '99.99', 'This is a product.', '"img/products/wired/wired4.jpg"', '"img/products/wired/thumb/wired4_sm.jpg"', 'Wired Phone'),
(19, 'Wired Phone 5', '1225.00', 'This is a product.', '"img/products/wired/wired5.jpg"', '"img/products/wired/thumb/wired5_sm.jpg"', 'Wired Phone'),
(31, 'asfd', '123.24', 'asf', 'img/products/brick/176070.jpg', 'img/products/brick/thumb/7686613.jpg', 'Brick Phone');

-- --------------------------------------------------------

--
-- Table structure for table `ecomm_user`
--

CREATE TABLE `ecomm_user` (
  `ecomm_user_id` int(11) NOT NULL,
  `user_full_name` text NOT NULL,
  `ecomm_user_email` varchar(75) NOT NULL,
  `ecomm_username` varchar(50) NOT NULL,
  `ecomm_password` varchar(50) NOT NULL,
  `user_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecomm_user`
--

INSERT INTO `ecomm_user` (`ecomm_user_id`, `user_full_name`, `ecomm_user_email`, `ecomm_username`, `ecomm_password`, `user_role`) VALUES
(1, 'John Doe', 'kpopelka@gmail.com', 'admin', 'admin', 1),
(2, 'Jane Joe', 'kpopelka@gmail.com', 'editor', 'Editor1', 2),
(3, 'Gary Doe', 'kpopelka@gmail.com', 'customer', 'customer', 3),
(8, 'Test Tester', 'testing@tests.com', 'testz', 'Testing1', 3),
(11, 'tsadf;lkjasf', 'asdfasdflk@asdflkasdf.com', 'kalsfasklfd', 'Testing1', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ecomm_contact_messages`
--
ALTER TABLE `ecomm_contact_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `ecomm_newsletter`
--
ALTER TABLE `ecomm_newsletter`
  ADD PRIMARY KEY (`newsletter_id`);

--
-- Indexes for table `ecomm_products`
--
ALTER TABLE `ecomm_products`
  ADD PRIMARY KEY (`product_number`);

--
-- Indexes for table `ecomm_user`
--
ALTER TABLE `ecomm_user`
  ADD PRIMARY KEY (`ecomm_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ecomm_contact_messages`
--
ALTER TABLE `ecomm_contact_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ecomm_newsletter`
--
ALTER TABLE `ecomm_newsletter`
  MODIFY `newsletter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ecomm_products`
--
ALTER TABLE `ecomm_products`
  MODIFY `product_number` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `ecomm_user`
--
ALTER TABLE `ecomm_user`
  MODIFY `ecomm_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

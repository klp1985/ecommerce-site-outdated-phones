<?php
    session_start();
    //Bring in Email and Validation classes
    include 'classes/userContact.php';
		include 'classes/validations.php';

    $validForm = false;

    $displayMessages = "";
    $inResponseMsg = "";
    $resultResponseMsg = "";

    if ($_SESSION['validUser'] == "yes") {	//If user is logged in already
			if ($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 2) {	//This is an admin or and editor.

        include 'connectPDO.php';

        $messageId = $_GET['message_id'];

        //Get message information from messages table based on the message id
        $sql = "SELECT message_sender_name, message_sender_email, message_subject, message_body FROM ecomm_contact_messages WHERE message_id = $messageId";

        try {

          $stmtMsg = $conn->prepare($sql);
          $stmtMsg->execute();

          if ($stmtMsg->execute()) {

            while($row = $stmtMsg->fetch()) {
              $messageSender = $row['message_sender_name'];
              $messageEmail = $row['message_sender_email'];
              $messageSubject = $row['message_subject'];
              $messageBody = $row['message_body'];
            }
            $conn = null;

            //Format message view
            $displayMessage = "<div class = 'container'>";
            $displayMessage .= "<div class = 'col-sm-3' style = 'border: 2px solid black;'>";
            $displayMessage .= "<h4>Sender Name:</h4><p>$messageSender</p><hr />";
            $displayMessage .= "<h4>Sender Email:</h4><p>$messageEmail</p><hr />";
            $displayMessage .= "<h4>Subject:</h4><p>$messageSubject</p>";
            $displayMessage .= "</div>";
            $displayMessage .= "<div class = 'col-sm-9' style = 'border: 2px solid black;'>";
            $displayMessage .= "<h4>Message:</h4><hr /><p>$messageBody</p>";
            $displayMessage .= "</div>";
            $displayMessage .= "<p><a href = 'login.php'>Go Back to Panel</a></p>";
            if ($_SESSION['userRole'] == 1) {
                $displayMessage .= "<p><a href = 'deletePage.php?keyid=$messageId&tname=ecomm_contact_messages'>Delete Message</a></p>";
            }
            $displayMessage .= "</div>";

          } else {
              $displayMessages = "There was an error collecting the message.";
          }

        } catch (PDOException $e) {
            echo "There was an error collecting the message." . $e->getMessage();
        }

      }
    } else {
      header('Location: login.php');
    }

    if ( isset($_POST['submitBtn']) ) {

				$emailUser = new User_Contact();
				$validations = new Validations();

				$inResponseMsg = $_POST['response'];

				$validForm = true;

				$validations->set_userMsg($inResponseMsg);
        $validations->set_validForm($validForm);

				$resultResponseMsg = $validations->validateResponseMsg();

				$validForm = $validations->get_validForm();
		}

?>

<!DOCTYPE html>
  <html lang="en">
    <head>
      <title>Products - Outdated Phones</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
      <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
    	<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

      <style>

      </style>
    </head>

    <body>

        <div class="jumbotron">
          <div class="container text-center">
            <h1>Outdated Phones</h1>
            <p>We sell everything but smartphones!</p>
          </div>
        </div>

        <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
                <li><a href="storeHome.php">Home</a></li>
                <li class="active"><a href="storeProducts.php">Products</a></li>
                <li><a href="storeContact.php">Contact</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
        						<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

		
						<li>
              <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                  <!-- Identify your business so that you can collect the payments. -->
                  <input type="hidden" name="business" value="kpopelka-seller@gmail.com">

                  <!-- Specify a PayPal shopping cart View Cart button. -->
                  <input type="hidden" name="cmd" value="_cart">
                  <input type="hidden" name="display" value="1">

                  <!-- Display the View Cart button. -->
                  <input type="image" name="submit" 
                    src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
                  alt="Add to Cart" style="margin-top:10px;">
                  <img alt="" width="1" height="1"
                    src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
                </form>          
            </li>
						</ul>
					</div>
					</div>
				</nav>

        <!--Place formatted view on page -->
        <?php echo $displayMessage; ?>

        <hr />

        <!-- Response message -->
          <div class="container">

<?php
      if ($validForm) { // If message passes validation then send information to Email class
          $emailUser->set_name($messageSender);
          $emailUser->set_email($messageEmail);
          $emailUser->set_subject($messageSubject);
          $emailUser->set_responseMsg($inResponseMsg);
          $emailUser->sendResponseEmail(); // Send response email
          echo $emailUser->formatHTMLResponseMessage();  // Show result message to user
      } else {
?>
          <form name="responseForm" method="post" action="messageView.php?message_id=<?php echo $messageId;?>">
            <div class="col-md-12">
              <div class="form-group">
                <label for="name">
                  Response:</label>
                  <span class = "error"><?php echo $resultResponseMsg; ?></span><br>
                <textarea name="response" id="message" class="form-control" rows="9" cols="25"
                  placeholder="Message"><?php echo $inResponseMsg;?></textarea>
              </div>
            </div>

            <div class="col-md-12">
              <button type="submit" class="btn btn-primary pull-right" id="btnContactUs" name = "submitBtn">
                Send Message</button>
            </div>
            </form>
<?php
      }
?>
          </div>

        <p>&nbsp;</p>

        <footer class="container-fluid text-center">
          <p>WDV 341: Intro to PHP Final Project</p>

        </footer>

      </body>
  </html>

<?php
		session_start();
		//Bring in the Email and Validation classes
		include 'classes/userContact.php';
		include 'classes/validations.php';

		if (!isset($_SESSION['validUser'])) {
				$_SESSION['validUser'] = "";
		}

		//Initialize variables
		$validForm = false;

		$inFullName = "";
		$inUsername = "";
		$inEmail = "";
		$inPassword = "";
		$inPasswordConfirm = "";

		$fullNameErrMsg = "";
		$usernameErrMsg = "";
		$emailErrMsg = "";
		$passwordErrMsg = "";
		$passwordConfirmErrMsg = "";

		$resultName = "";
		$resultEmail = "";
		$resultUsername = "";
		$resultPassword = "";
		$resultPasswordConfirm = "";

		$resultMsg = "";
		$resultNewsletterMsg = "";

		if ( isset($_POST['submitBtn']) ) {				//Checks if the form has been submitted.  If it has it will validate the form.
				$validations = new Validations();

				$inFullName = $_POST['fullname'];
				$inUsername = $_POST['username'];
				$inEmail = $_POST['email'];
				$inPassword = $_POST['password'];
				$inPasswordConfirm = $_POST['password_confirm'];

				$validForm = true;

				$validations->set_name($inFullName);
				$validations->set_email($inEmail);
				$validations->set_username($inUsername);
				$validations->set_password($inPassword);
				$validations->set_passwordConfirm($inPasswordConfirm);
				$validations->set_validForm($validForm);

				$resultName = $validations->validateName();
				$resultEmail = $validations->validateEmail();
				$resultUsername = $validations->validateUsername();
				$resultPassword = $validations->validatePassword();
				$resultPasswordConfirm = $validations->validatePasswordConfirm();
				$resultPhone = $validations->validatePhauxn();

				$validForm = $validations->get_validForm();
				$inFullName = $validations->get_name();
				$inEmail = $validations->get_email();
				$inUsername = $validations->get_username();
				$inPassword = $validations->get_password();
				$inPasswordConfirm = $validations->get_passwordConfirm();
		}

		if ($validForm) { //If form passes validation

			include "connectPDO.php";  //Connects to the database and inserts the data into the users table.
			try {
					if (isset($_POST['newsletter'])) { //Checks if newsletter checkbox has been selected.  If so, inserts submitted email address into newsletter table.
								$sqlNewsletter = "INSERT INTO ecomm_newsletter (newsletter_email) VALUES (:newsletter_email)";

								$sqlPrepNews = $conn->prepare($sqlNewsletter);

								$sqlPrepNews->bindParam(':newsletter_email', $inEmail);

								if (!$sqlPrepNews->execute()){
										$resultNewsletterMsg = "An error occurred adding you to the newletter list.";
								}
					}

					$sql = "INSERT INTO ecomm_user (user_full_name, ecomm_user_email, ecomm_username, ecomm_password, user_role) VALUES (:user_full_name, :ecomm_user_email, :ecomm_username, :ecomm_password, 3)";

					$sqlPrepare = $conn->prepare($sql);

					$sqlPrepare->bindParam(':user_full_name', $inFullName);
					$sqlPrepare->bindParam(':ecomm_user_email', $inEmail);
					$sqlPrepare->bindParam(':ecomm_username', $inUsername);
					$sqlPrepare->bindParam(':ecomm_password', $inPassword);
			}

			catch (PDOException $e) {
					echo "There was a problem entering the information.  Please try again: " . $e->getMessage();
			}

			$conn = null;

			if ($sqlPrepare->execute()){

					$resultMsg = "<h2>Thank You, $inFullName, for becoming a member!</h2>";
					$resultMsg .= "<p>A confirmation email has been sent to $inEmail.";
					$resultMsg .= "<p>You now have access to your customer panel where you can update your information or view your orders.  <a href = 'login.php'>Login now</a> to go there.</p>";

			} else {
					$resultMsg = "<h3>A Small Problem Occurred.</h3>";
					$resultMsg .= "<p>There was an error processing your information.</p>";
					$resultMsg .= "<p><a href = 'storeRegistration.php'>Please try again.</a></p>";
			}
		}
 ?>

<!DOCTYPE html>
	<html lang="en">
		<head>
			<title>New User Signup - Outdated Phones</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
			<link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
			<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

			<script>

					function checkPasswordMatch() {  //Front end validation. Checks if password fields have exact same input.
							var pass1 = document.getElementById("password").value;
							var pass2 = document.getElementById("password_confirm").value;
							var passwordMatchYes = true;

							if (pass1 != pass2 && pass1 != "" && pass2 != "") {
									passwordMatchYes = false;

									document.getElementById("password").style.borderColor = "#f44256";
            			document.getElementById("password_confirm").style.borderColor = "#f44256";
									document.getElementById("passMsg").innerHTML = "<p style = 'color: #f44256; font-size: 1.5em;'>The passwords do not match!</p>";
							} else if (pass1 != "" && pass2 != ""){
									document.getElementById("password").style.borderColor = "#00cc77";
									document.getElementById("password_confirm").style.borderColor = "#00cc77";
									document.getElementById("passMsg").innerHTML = "<p style = 'color: #00cc77; font-size: 1.5em;'>Passwords Match!</p>";
							} else if (pass1 == "" || pass2 == "") {
									document.getElementById("password").style.borderColor = "#b2b2b2";
									document.getElementById("password_confirm").style.borderColor = "#b2b2b2";
									document.getElementById("passMsg").innerHTML = "";
							}
					}

			</script>

			<style>

			</style>
		</head>


		<body>

			<div class="jumbotron">
			  <div class="container text-center">
				<h1>Outdated Phones</h1>
				<p>We sell everything but smartphones!</p>
			  </div>
			</div>

			<nav class="navbar navbar-inverse">
			  <div class="container-fluid">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
				  <ul class="nav navbar-nav">
					<li class="active"><a href="storeHome.php">Home</a></li>
					<li><a href="storeProducts.php">Products</a></li>
					<li><a href="storeContact.php">Contact</a></li>
				  </ul>
				  <ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
	<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
	?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
	<?php
								} else {
	?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
	<?php
								}
	?>
						</a></li>

						<li>
							<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<!-- Identify your business so that you can collect the payments. -->
									<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

									<!-- Specify a PayPal shopping cart View Cart button. -->
									<input type="hidden" name="cmd" value="_cart">
									<input type="hidden" name="display" value="1">

									<!-- Display the View Cart button. -->
									<input type="image" name="submit" 
										src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
									alt="Add to Cart" style="margin-top:10px;">
									<img alt="" width="1" height="1"
										src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
								</form>					
						</li>
						</ul>
					</div>
					</div>
				</nav>
<?php
		if ($validForm) {
				echo $resultMsg;
		} else {
?>
			<div class="container">
				<div class="row">
					<div class="col-md-6">

						<form class="form-horizontal" action="storeRegistration.php" method="POST">
						  <fieldset>
							<div id="legend">
							  <legend class="">Register</legend>
							</div>

							<div class="control-group">
							  <label class="control-label" for="fullname">Your Name</label><span class = "error"><?php echo $resultName;?></span>
							  <div class="controls">
								<input id="fullname" name="fullname" placeholder="" class="form-control input-lg" type="text" value="<?php echo $inFullName;?>" />
								<p class="help-block">Please provide your first and last name.</p>
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="username">Username</label><span class = "error"><?php echo $resultUsername;?></span>
							  <div class="controls">
								<input id="username" name="username" placeholder="" class="form-control input-lg" type="text" value="<?php echo $inUsername;?>" />
								<p class="help-block">Username must be at least 5 characters and can contain any letters or numbers, without spaces</p>
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="email">E-mail</label><span class = "error"><?php echo $resultEmail;?></span>
							  <div class="controls">
								<input id="email" name="email" placeholder="" class="form-control input-lg" type="email" value="<?php echo $inEmail;?>" />
								<p class="help-block">Please provide your E-mail</p>
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="password">Password</label><span class = "error"><?php echo $resultPassword;?></span>
							  <div class="controls">
								<input id="password" name="password" placeholder="" class="form-control input-lg" type="password" onchange = "checkPasswordMatch();" value="<?php echo $inPassword;?>" />
								<p class="help-block">Password should be at least 6 characters</p>
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="password_confirm">Password (Confirm)</label><span class = "error"><?php echo $resultPasswordConfirm;?></span>
							  <div class="controls">
								<input id="password_confirm" name="password_confirm" placeholder="" class="form-control input-lg" type="password" onchange = "checkPasswordMatch();" value="<?php echo $inPasswordConfirm;?>" /><span id = "passMsg"></span>
								<p class="help-block">Please confirm password</p>
							  </div>
							</div>

							<p class = "containField">
										<label>Phone:  <br>
											  <input type="text" name="phone" id="phone">
										</label>
							</p>

							<div class="control-group">
							  <label class="control-label" for="newsletter">Newsletter Signup</label>
							  <div class="controls">
								<input type="checkbox" name = "newsletter" data-reverse>
								<p class="help-block">If this box is checked, you will receive our weekly newsletter.</p>
							  </div>
							</div>


							<div class="control-group">
							  <!-- Button -->
							  <div class="controls">
								<input type = "submit" name = "submitBtn" id="submitBtn" class="btn btn-success" value = "Register" />
							  </div>
							</div>
						  </fieldset>
						</form>

					</div>
				</div>
			</div><br>
<?php
		}
?>
			<div class="container">

			</div><br><br>

			<footer class="container-fluid text-center">

			</footer>

		</body>
	</html>

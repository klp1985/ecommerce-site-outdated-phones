<?php

    //The sqlDelete class will delete a record from a database table.

		class sqlDelete {

				//Properties
						public $rowIdValue;

            public $tableName;

				//Setters

						function set_rowId($inRowIdValue) {
								$this -> rowIdValue = $inRowIdValue;  //Put input value into property
						}


						function set_tableName($inTableName) {
								$this -> tableName = $inTableName;  //Put input value into property
						}

				//Getters

						function get_rowIdValue() {
								return $this -> rowIdValue;
						}


						function get_tableName() {
								return $this -> tableName;
						}

				//Processing Methods

						function deleteRowFromDB() {

              $message = "";

              include 'connectPDO.php';		//connects to the database

              $primaryKeyColName = "";

              try {
                $sqlTry = "SELECT * FROM $this->tableName LIMIT 0";
                $result = $conn->query($sqlTry);
                  for ($i = 0; $i < $result->columnCount(); $i++) {  //Get all of the column names from the table.
                    $col = $result->getColumnMeta($i);
                    $columns[] = $col['name'];
                  }
                  //print_r($columns);
                  $primaryKeyColName = $columns[0];  //Get the column name of the first column, which is the column the primary key is in in all tables of the ecommsite database.
                  //echo $primaryKeyColName;

              }
              catch (PDOException $e) {
                  $message = "<p style = 'color: red; text-align: center;'>There was a problem finding the record you want to delete.  Please try again" . $e->getMessage() . "</p>";
              }

              try {
                  $sql = "DELETE FROM $this->tableName WHERE $primaryKeyColName = $this->rowIdValue";
                  //echo "<p>The SQL Command: $sql </p>";     //testing

                  $conn->exec($sql);

                  $message =  "<h4 style = 'color: #42f442; text-align: center;'>The record was successfully deleted.</h4>";
              }

              catch (PDOException $e) {
                  $message = "<p style = 'color: red; text-align: center;'>There was a problem deleting the record.  Please try again" . $e->getMessage() . "</p>";
              }
                $conn = null;

								return $message;
						}

		} //End Class

?>

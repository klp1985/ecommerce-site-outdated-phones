<?php
    session_start();

    $displayProducts = "";

    if ($_SESSION['validUser'] == "yes") {
      if ($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 2) {

        include 'connectPDO.php';

        //Get all product information from products table.
        $sqlProducts = "SELECT product_title, product_price, product_description, product_image_path, product_thumb_path, product_type, product_number FROM ecomm_products";

        try {
              $stmt = $conn->prepare($sqlProducts);
              $stmt->execute();

              if ($stmt->execute()) {
                //Format table
                  $displayProducts .= "<div class = 'container'>";
                  $displayProducts .= "<h3>Products</h3>";
                  $displayProducts .= "<div class = 'row'>";
                  $displayProducts .= "<div class = 'col-sm-12'>";
                  $displayProducts .= "<table class='tg'>";
                  $displayProducts .= "<tr>";
                  $displayProducts .= "<th class='tg-aq88'>Product Number</th>";
                  $displayProducts .= "<th class='tg-aq88'>Product Title</th>";
                  $displayProducts .= "<th class='tg-aq88'>Product Type</th>";
                  $displayProducts .= "<th class='tg-aq88'></th>";
                  if ($_SESSION['userRole'] == 1) {
                    $displayProducts .= "<th class='tg-wr1b'></th>";
                  }
                  $displayProducts .= "</tr>";

                  while($row = $stmt->fetch()) {
                    // Place product information into table for each product
                      $displayProducts .= "<tr>";
                      $displayProducts .= "<td class='tg-yzt1'>" . $row['product_number'] . "</td>";
                      $displayProducts .= "<td class='tg-yzt1'>" . $row['product_title'] . "</td>";
                      $displayProducts .= "<td class='tg-yzt1'>" . $row['product_type'] . "</td>";
                      $displayProducts .= "<td class='tg-yzt1'><a href='add-updateProduct.php?prod_no=" . $row['product_number'] . "'>Update</a></td>";
                      if ($_SESSION['userRole'] == 1) {
                        $displayProducts .= "<td class='tg-yzt1'><a href='deletePage.php?keyid=" . $row['product_number'] . "&tname=ecomm_products'>Delete</a></td>";
                      }
                      $displayProducts .= "</tr>\n";

                  }

                  $displayProducts .= "</table>";
                  $displayProducts .= "<p style = 'margin-top: 25px;'><a href='add-updateProduct.php' class='btn btn-info btn-lg'>Add A Product</a><a href='login.php' class='btn btn-info btn-lg' style = 'margin-left: 25px;'>Go Back</a></p>";
                  $displayProducts .= "</div>";
                  $displayProducts .= "</div>";
                  $displayProducts .= "</div>";

                  $_SESSION['products'] = $displayProducts;

                  $conn = null;
              } else {
                  $displayMessages = "There was an error collecting the product.";
              }
          }
          catch (PDOException $e) {
              echo "There was an error collecting the products." . $e->getMessage();
          }
      }
    } else {
      header('Location: login.php');
    }
?>

<!DOCTYPE html>
		<html>
				<head>

						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

						<title>Manage Products - Outdated Phones</title>

						<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
						<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
						<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
						<link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
						<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

						<style>

						</style>

				</head>

				<body>

						<div class="jumbotron">
						  <div class="container text-center">
							<h1>Outdated Phones</h1>
							<p>We sell everything but smartphones!</p>
						  </div>
						</div>

						<nav class="navbar navbar-inverse">
						  <div class="container-fluid">
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
							  <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
							</div>
							<div class="collapse navbar-collapse" id="myNavbar">
							  <ul class="nav navbar-nav">
								<li class="active"><a href="storeHome.php">Home</a></li>
								<li><a href="storeProducts.php">Products</a></li>
								<li><a href="storeContact.php">Contact</a></li>
							  </ul>
							  <ul class="nav navbar-nav navbar-right">

						      <li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

						<li>
							<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<!-- Identify your business so that you can collect the payments. -->
									<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

									<!-- Specify a PayPal shopping cart View Cart button. -->
									<input type="hidden" name="cmd" value="_cart">
									<input type="hidden" name="display" value="1">

									<!-- Display the View Cart button. -->
									<input type="image" name="submit" 
										src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
									alt="Add to Cart" style="margin-top:10px;">
									<img alt="" width="1" height="1"
										src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
								</form>					
						</li>
						</ul>
					</div>
					</div>
				</nav>
        <!-- Place products table on page -->
        <?php echo $displayProducts;?>

				</body>
		</html>

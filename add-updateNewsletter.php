<?php
		session_start();

		$validForm = false;

    $inNewsEmail = "";
    $inNewsId = "";

		$updateHeading = "";
		$resultMsg = "";

		$newsEmailErrMsg = "";


    include 'classes/validations.php';


	if ($_SESSION['validUser'] == "yes") {
    if ($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 2) {

      include "connectPDO.php";  //Connects to the database and inserts the data

      if ( isset($_POST['submitBtn']) ) {	 /*If the form has been posted or submitted*/

          $validations = new Validations();

        /*Get the form data from the $_POST variable and place in PHP variables*/

          $inNewsEmail = $_POST['newsletter_email'];


          if (isset($_GET['newsletter_id'])) {
        /*Get the event ID from the $_GET variable that was passed from the action attribute in the form*/
              $inNewsId = $_GET['newsletter_id'];
              $updateHeading = "<h1>Updating ID:  $inNewsId</h1>";
          } else {
              $updateHeading = "<h1>Add Email Address:</h1>";
          }
        /*Validate the form data or at least sanitize the data*/
          $validForm = true;

  				$validations->set_email($inNewsEmail);
  				$validations->set_validForm($validForm);

  				$newsEmailErrMsg = $validations->validateEmail();

  				$validForm = $validations->get_validForm();
  				$inNewsEmail = $validations->get_email();


      } //End if form submitted

      elseif (isset($_GET['newsletter_id'])) {
          //If there is a product number value in the Get variable
          //Do update

          $inNewsId = $_GET['newsletter_id'];

          $updateHeading = "<h1>Updating ID:  $inNewsId</h1>";

          /*Create a SELECT query to retrieve the requested record from the database table*/

          $sqlSelect = "SELECT newsletter_id, newsletter_email FROM ecomm_newsletter WHERE newsletter_id = $inNewsId";

          //Run SELECT query

          try {
              $stmt = $conn->prepare($sqlSelect);

              $stmt->execute();
          } catch (PDOException $e) {

              $resultMsg = "<p>There was an error with your request: " . $e->getMessage() . "</p>";
          }

          if ($stmt->execute()){  /*If select query was successful and there is content*/
          //Place content from database into form fields
          //Display the form to the user with content in the fields
              while($row = $stmt->fetch()) {

                  $inNewsEmail = $row['newsletter_email'];

              }
          }
      } else {
          $updateHeading = "<h1>Add Email Address:</h1>";
      }

    	if ($validForm) {  /*If data validation passes...*/
        if (isset($_GET['newsletter_id'])) {  // if product number is present in Get variable then update the info in database with new values
    				$sqlUpdate = "UPDATE ecomm_newsletter SET ";
    				$sqlUpdate .= "newsletter_email = '$inNewsEmail' ";
    				$sqlUpdate .= "WHERE (newsletter_id = '$inNewsId')";

    				$updateStmt = $conn->prepare($sqlUpdate);

    				//Run the update query

    				$updateStmt->execute();

    				if ($updateStmt->execute()){  /*If update query was successful*/

    						//Confirm to the user that their data has been successfully processed
    						//Provide a link to the home page and the select page
    						$resultMsg =  "<h1>Success! The email address has been updated.</h1>";
                $resultMsg .= "<p><a class='btn btn-info btn-lg' href = 'add-updateNewsletter.php'>Add An Email</a>";
                $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'manageNewsletter.php'>Back to Email List</a>";
                $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'logout.php'>Logout</a></p>";
    				}

    				else { /*If there was an error with the query and did not execute*/
    						$resultMsg = "<h1>Update error: " . $e->getMessage() . "</h1>";
    						//Display message “Oops Please try again”
    						//Display a link to the home page and select page

    				}
          } elseif (!isset($_GET['newsletter_id'])) { // add product to database
            try {

              $sqlAdd = "INSERT INTO ecomm_newsletter (newsletter_email) VALUES (:newsletter_email)";

              $sqlPrepareAdd = $conn->prepare($sqlAdd);

              $sqlPrepareAdd->bindParam(':newsletter_email', $inNewsEmail);

            } catch (PDOException $e) {
                echo "There was a problem entering the information.  Please try again: " . $e->getMessage();
            }

            if ($sqlPrepareAdd->execute()){

                  $resultMsg = "<h4>The new email address has been added.</h4>";
                  $resultMsg .= "<p><a class='btn btn-info btn-lg' href = 'add-updateNewsletter.php'>Add Another Email</a>";
                  $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'manageNewsletter.php'>Back to Email List</a>";
                  $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'logout.php'>Logout</a></p>";
            }
        }
    	}

    } else {

  			header('Location: login.php');

  	}
	} else {

			header('Location: login.php');

	}


?>
<!DOCTYPE html>
	<html lang="en">
		<head>
		  <title>Add/Update Newsletter List - Outdated Phones</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">

		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
		  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
			<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

		  <style>

		  </style>
		</head>


		<body>

				<div class="jumbotron">
				  <div class="container text-center">
				    <h1>Outdated Phones</h1>
				    <p>We sell everything but smartphones!</p>
				  </div>
				</div>

				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
				    </div>
				    <div class="collapse navbar-collapse" id="myNavbar">
				      <ul class="nav navbar-nav">
				        <li><a href="storeHome.php">Home</a></li>
				        <li><a href="storeProducts.php">Products</a></li>
				        <li><a href="storeContact.php">Contact</a></li>
				      </ul>
				      <ul class="nav navbar-nav navbar-right">

										<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

						<li>
							<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<!-- Identify your business so that you can collect the payments. -->
									<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

									<!-- Specify a PayPal shopping cart View Cart button. -->
									<input type="hidden" name="cmd" value="_cart">
									<input type="hidden" name="display" value="1">

									<!-- Display the View Cart button. -->
									<input type="image" name="submit" 
										src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
									alt="Add to Cart" style="margin-top:10px;">
									<img alt="" width="1" height="1"
										src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
								</form>					
						</li>
						</ul>
					</div>
					</div>
				</nav>

        <div class = "container">
<?php
		//If the user submitted the form the changes have been made
		//if ( isset($_POST['submitBtn']) ) {
      if ($validForm) {
				echo $resultMsg;	//contains a Success or Failure output content
      }
		//}//end if submitted

		else
		{	//The page needs to display the form and associated data to the user for changes
				echo $updateHeading;
?>
								 <form id="form1" name="Add-Update_News" method="post" action="add-updateNewsletter.php<?php if (isset($_GET['newsletter_id'])){echo '?newsletter_id='.$inNewsId;}?>">

                   <div class = "row">
                     <div class = "col-sm-5">

		                    <p>User Email:  <span class = "error"><?php echo "$newsEmailErrMsg"; ?></span><br>
														<input type="text" name="newsletter_email" id="newsletterEmail" value="<?php echo $inNewsEmail;?>"/>
												</p>

												<p>
														<input type="submit" class="btn btn-info btn-lg" name="submitBtn" id="submitBtn" value="Submit" />
														<input type="reset" class="btn btn-info btn-lg" name="resetBtn" id="resetBtn" value="Clear Form" />
												</p>
												
                  	</div>
                  </div>
								</form>
                <hr />
								<p>
										<a class="btn btn-info btn-lg" href = "manageNewsletter.php">Go Back to Table</a>

										<a class="btn btn-info btn-lg" style = "margin-left: 15px;" href='logout.php'>Logout of Admin System</a>
								</p>

						</div>

<?php
		}//end else submitted

		$conn = null;

?>
				</body>

		</html>
